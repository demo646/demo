import { types } from '../types';

const initialState = {
  loading: false,
  todos: [],
};

const totoReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_TODO:
      return {
        ...state,
        loading: true,
      };
    case types.GET_TODO_SUCCESS:
      return {
        ...state,
        todos: action.payload,
        loading: false,
      };
    case types.GET_TODO_FAIL:
      return {
        ...state,
        loading: false,
      };
    case types.ADD_TODO:
      return {
        ...state,
        loading: true,
      };
    case types.ADD_TODO_SUCCESS:
      return {
        ...state,
        todos: [action.payload, ...state.todos],
        loading: false,
      };
    case types.ADD_TODO_FAIL:
      return {
        ...state,
        loading: false,
      };
    case types.REMOVE_TODO:
      return {
        ...state,
        loading: true,
      };
    case types.REMOVE_TODO_SUCCESS:
      return {
        ...state,
        todos: state.todos.filter((todo) => todo.id !== action.payload),
        loading: false,
      };
    case types.REMOVE_TODO_FAIL:
      return {
        ...state,
        loading: false,
      };
    default:
      return state;
  }
};

export default totoReducer;
