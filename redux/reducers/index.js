import { combineReducers } from 'redux';
import totoReducer from './totoReducer';

export default combineReducers({
  todo: totoReducer,
});
