import { call, put, all, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { types } from '../types';
import {
  getTodoFail,
  getTodoSucces,
  addTodoSucces,
  addTodoFail,
  removeTodoSucces,
  removeTodoFail,
} from '../actions/todoAction';

export function* getTodoSaga() {
  try {
    const res = yield axios.get(
      'https://jsonplaceholder.typicode.com/todos?&_limit=10'
    );

    yield put(getTodoSucces(res.data));
  } catch (error) {
    yield put(getTodoFail(error));
  }
}

export function* addTodoSagaAsync(action) {
  try {
    const res = yield axios.post(
      'https://jsonplaceholder.typicode.com/todos',
      action.payload
    );

    yield put(addTodoSucces(res.data));
  } catch (error) {
    yield put(addTodoFail(error));
  }
}

export function* removeTodoSagaAsync(action) {
  try {
    yield axios.delete(
      `https://jsonplaceholder.typicode.com/todos/${action.payload}`
    );

    yield put(removeTodoSucces(action.payload));
  } catch (error) {
    yield put(removeTodoFail(error));
  }
}

export function* watchTodo() {
  yield takeEvery(types.GET_TODO, getTodoSaga);
}

export function* addTodoSaga() {
  yield takeEvery(types.ADD_TODO, addTodoSagaAsync);
}

export function* removeTodoSaga() {
  yield takeEvery(types.REMOVE_TODO, removeTodoSagaAsync);
}

export function* todoSaga() {
  yield all([watchTodo(), addTodoSaga(), removeTodoSaga()]);
}
