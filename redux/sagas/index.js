import { all } from '@redux-saga/core/effects';
import { todoSaga } from './totoSaga';

export default function* () {
  yield all([todoSaga()]);
}
