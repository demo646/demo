import { types } from '../types';

export const getTodo = () => ({
  type: types.GET_TODO,
});

export const getTodoSucces = (data) => ({
  type: types.GET_TODO_SUCCESS,
  payload: data,
});

export const getTodoFail = (error) => ({
  type: types.GET_TODO_FAIL,
  payload: error.message,
});

export const addTodo = (data) => ({
  type: types.ADD_TODO,
  payload: data,
});

export const addTodoSucces = (data) => ({
  type: types.ADD_TODO_SUCCESS,
  payload: data,
});

export const addTodoFail = (error) => ({
  type: types.ADD_TODO_FAIL,
  payload: error.message,
});

export const removeTodo = (id) => ({
  type: types.REMOVE_TODO,
  payload: id,
});

export const removeTodoSucces = (id) => ({
  type: types.REMOVE_TODO_SUCCESS,
  payload: id,
});

export const removeTodoFail = (error) => ({
  type: types.REMOVE_TODO_FAIL,
  payload: error.message,
});
