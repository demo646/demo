import * as React from 'react';
import Box from '@mui/material/Box';
import LinearProgress from '@mui/material/LinearProgress';
import Backdrop from '@mui/material/Backdrop';

export default function LinearLoading() {
  return (
    <Box sx={{ width: '100%' }}>
      <LinearProgress />
      <Backdrop
        sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={true}
      />
    </Box>
  );
}
