import React, { useEffect, createRef, useRef, useState } from 'react';
import { useSelector, useDispatch, connect } from 'react-redux';
import {
  Button,
  Container,
  Grid,
  TextField,
  Typography,
  CircularProgress,
  List,
  ListItem,
  ListItemText,
  IconButton,
} from '@mui/material';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';

import { addTodo, getTodo, removeTodo } from '../../redux/actions/todoAction';
import LinearLoading from '../../src/components/LinearLoading';

const Todos = () => {
  const dispatch = useDispatch();
  const todo = useSelector((state) => state.todo);
  const [text, setText] = useState('');

  const inputRef = useRef();

  useEffect(() => {
    dispatch(getTodo());
  }, [dispatch]);

  const handleClickTodo = () => {
    if (text.length < 3 || text.length > 15) {
      return;
    }

    let data = {
      userId: 1,
      title: text,
      completed: false,
    };
    dispatch(addTodo(data));
    setText('');
    inputRef.current.focus();
  };

  return (
    <>
      {/* {todo.loading && <LinearLoading />} */}
      <Container maxWidth="sm" sx={{ mt: 2 }}>
        <Typography align="center" variant="h2" component="h1">
          Todos
        </Typography>
        <Grid
          container
          spacing={2}
          alignItems="center"
          justifyContent="space-between"
        >
          <Grid item xs={9}>
            <TextField
              variant="outlined"
              fullWidth
              name="todo"
              value={text}
              onChange={(e) => setText(e.target.value)}
              placeholder="Enter your todo"
              inputRef={inputRef}
              margin="normal"
              size="normal"
            />
          </Grid>
          <Grid item xs={3}>
            <Button
              fullWidth
              variant="contained"
              color="primary"
              onClick={handleClickTodo}
              disabled={todo.loading}
              size="large"
            >
              Add Todo
            </Button>
          </Grid>
        </Grid>
        {todo.loading && (
          <CircularProgress sx={{ mt: 5 }} size={50} color="secondary" />
        )}
        <List>
          {!todo.loading &&
            todo.todos.map((item, i) => (
              <ListItem key={item.id + item.title + i}>
                <ListItemText primary={item.title} />
                <IconButton onClick={() => dispatch(removeTodo(item.id))}>
                  <DeleteForeverIcon />
                </IconButton>
              </ListItem>
            ))}
        </List>
      </Container>
    </>
  );
};

export default Todos;

// class verson
// class Todos extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       todo: '',
//     };
//     this.inputRef = createRef();
//   }

//   componentDidMount() {
//     this.props.getTodo();
//   }

//   handleClickTodo = () => {
//     this.data = {
//       userId: 1,
//       title: this.state.todo,
//       completed: false,
//     };
//     this.props.addTodo(this.data);
//     this.setState({ todo: '' });
//     this.inputRef.current.focus();
//   };

//   render() {
//     return (
//       <>
//         <h1>Todos</h1>
//         <div>
//           <input
//             name="todo"
//             value={this.state.todo}
//             onChange={(e) => this.setState({ todo: e.target.value })}
//             placeholder="Enter your todo"
//             ref={this.inputRef}
//           />
//           <button onClick={this.handleClickTodo}>Add Todo</button>
//         </div>
//         <ul>
//           {this.props.todo.loading && <div>Loading...</div>}
//           {!this.props.todo.loading &&
//             this.props.todo.todos.map((item) => (
//               <li key={item.id}>{item.title}</li>
//             ))}
//         </ul>
//       </>
//     );
//   }
// }

// const mapStateToProps = (state) => ({
//   todo: state.todo,
// });

// const mapDispatchToProps = (dispatch) => {
//   return {
//     getTodo: () => dispatch(getTodo()),
//     addTodo: (todo) => dispatch(addTodo(todo)),
//   };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Todos);
