import * as React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

import Link from '../src/Link';

export default function Index() {
  return (
    <Container maxWidth="sm">
      <Box sx={{ my: 4 }}>
        <Typography variant="h4" component="h1" gutterBottom>
          Simple todo using redux saga
        </Typography>
        <Link href="/todos" color="secondary">
          Go to the todo page
        </Link>
      </Box>
    </Container>
  );
}
